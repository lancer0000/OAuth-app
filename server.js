const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const morgan = require('morgan');
const expHbs = require('express-handlebars');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const app = express();
const routes = require('./routes/app-routes');

const port = process.env.PORT || 3000;

app.use(express.static(path.join(__dirname, 'public')));

app.use(morgan('dev'));


//handlebars setup
app.engine('hbs', expHbs({extname: 'hbs', defaultLayout: 'layout'}));
app.set('view engine', 'hbs');

//app routes
app.use(routes);

//catch 404 errors & forward them to the error handler
app.use((req, res, next) => {
    const error = new Error('404: Not found');
    error.status = 404;
    next(error);
});

//error handler
app.use((err, req, res, next) => {
    res.status(req.status || 500);
    // res.send('<h1>' + err.message + '</h1>');
    res.sendFile(path.join(__dirname, 'public/error.html'));
});

app.listen(port, () => {
    console.log(`Listening for requests on port ${port}`);
});